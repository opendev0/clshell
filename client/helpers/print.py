'''
This module defines some helpers for nicer output on the terminal.
'''

RED   = '\x1b[1;31m'
GREEN = '\x1b[1;32m'
BLUE  = '\x1b[1;34m'
RESET = '\x1b[0m'

def info(text):
    '''Prints an info message.'''
    print(f'{BLUE}[i]{RESET} {text}')

def error(text):
    '''Prints an error message.'''
    print(f'{RED}[f]{RESET} {text}')
