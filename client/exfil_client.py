'''
This module combines the input and output method and gives the possibility to set the local TTY
into raw mode to make the remote shell more comfortable.
'''

import os
import signal
import subprocess
from multiprocessing import Process, current_process

from terminal import TTYType, Terminal
from helpers.print import info

class ExfilClient:
    '''
    Combines the input and output method and gives the possibility to set the local TTY into raw
    mode to make the remote shell more comfortable.
    '''

    def __init__(self, input_handler, output_handler, tty_type=TTYType.medium):
        self.terminal = Terminal(tty_type)
        self.input = input_handler
        self.output = output_handler
        self.shell_input_thread = None
        self.shell_output_thread = None
        self.main_pid = None

        self.sync_tty_size()

        signal.signal(signal.SIGINT, self.__signal_handler)

    def run(self):
        '''
        Run the input and output threads.
        '''

        self.main_pid = os.getpid()

        # Output thread will keep the program alive. Input thread can be daemonized since it does
        # not handle any shared data references.
        # Input and output is handled in two threads, to allow the user to input new commands while
        # still receiving data (like in most shells).
        # TODO: Make this work with asyncio
        if self.terminal.tty_type == TTYType.medium:
            self.shell_input_thread = Process(target=self.input.handle_shell_input_buffered)
        else:
            self.shell_input_thread = Process(target=self.input.handle_shell_input)

        self.shell_output_thread = Process(target=self.output.handle_shell_output)

        self.shell_input_thread.start()
        self.shell_output_thread.start()

    def sync_tty_size(self):
        '''
        This method can be called to notify the remote TTY about the size of the local TTY.
        '''

        # TODO: This does not work right now.

        tty_size = subprocess.check_output(['stty', 'size']).strip().split(b' ')
        #self.input.shell_cmd(b'stty rows %s cols %s\n' % (tty_size[0], tty_size[1]))

    def __signal_handler(self, signum, _):
        # TODO: Question does not work
        if signum == signal.SIGINT and current_process().pid == self.main_pid:
            # answer = input('Are you sure you want to close the shell? [y/n] ')
            # if answer == 'y':
            self.__clean_exit()

    def __clean_exit(self):
        info('\rExiting...')

        self.shell_input_thread.terminate()

        # Output thread is daemonized to ensure it can be killed here. However, give the
        # implementor a chance to properly stop this thread.
        self.output.deinit_exfil()
        self.shell_output_thread.terminate()

        # Send the server a kill signal and restore the terminal.
        self.input.shell_cmd(b'\x03')
        self.terminal.restore()
