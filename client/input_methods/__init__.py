'''
This module defines input methods that can be used to get commands that should be executed on the
remote server through some kind of RCE to a named pipe that was opened by the server component of
this tool.
'''

__all__ = [
    'direct',
]
