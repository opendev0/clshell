'''
This module implements an input method for local testing.

It writes the shell command directly into the named pipe created by the server component on the
same host.
'''

import os
import stat
import shlex
from queue import Queue
from subprocess import run, DEVNULL

from .abstract import Input

class DirectInput(Input):
    '''
    Input method for local testing. Writes commands directly into the named pipe created by the
    server component.
    '''
    def __init__(self):
        super().__init__()
        self.cmd_queue = Queue(100)

    def remote_command(self, cmd):
        # Without this, the input method can not write to the named pipe if the client is executed as root which is necessary, e.g., when trying to sniff ICMP packets.
        # Since the named pipe is owned by the user and only the owner can write to it, root will not be able to write to the pipe.
        if os.getuid() == 0:
            cmd = f'runuser -u {os.getlogin()} -- {cmd}'

        run(shlex.split(cmd), stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL)

    def shell_cmd(self, cmd):
        try:
            if stat.S_ISFIFO(os.stat(self.pipe_path).st_mode):
                self.__exec_queue()
                super().shell_cmd(cmd)
        except IOError:
            # Queue commands if pipe does not exist yet
            self.cmd_queue.put(cmd, False)

    def __exec_queue(self):
        while not self.cmd_queue.empty():
            cmd = self.cmd_queue.get(False)
            super().shell_cmd(cmd)