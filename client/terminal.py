'''
This module defines a possibility to put the local terminal into raw mode and back.
'''

import sys
import subprocess

from helpers.print import info

class TTYType:
    '''
    Three TTY types are defined:

    * dumb: The local TTY is not changed. Using this, the remote shell is basically a normal netcat
      shell.
    * raw: The local TTY is set to raw mode. This way any keypress is directly sent to the remote
      server.
    * medium: The local TTY is not changed. However, the input is checked and a few keypresses lead
      to all buffered characters to be sent to the server. This way, not every character needs to
      be transmitted instantly (which can be expensive if the RCE is not trivial), but Tab or the
      arrow keys can still trigger the transmission of the input buffer and allow auto completion
      or history buffer usage.
    '''

    dumb = 0
    raw = 1
    medium = 2

class Terminal:
    '''
    Put the local terminal into raw mode and back.
    '''

    def __init__(self, tty_type):
        self.tty_type = tty_type

        if self.tty_type != TTYType.dumb:
            sys.stdout.buffer.write(b'Changed local TTY to raw\r\n')
            self.tty_state_backup = subprocess.check_output(['stty', '-g']).strip()
            subprocess.run(['stty', 'raw', '-echo'])

    def restore(self):
        '''
        Restore the local TTY from raw mode.
        '''
        if self.tty_type != TTYType.dumb:
            subprocess.run(['stty', self.tty_state_backup])
            info('Local TTY restored')
