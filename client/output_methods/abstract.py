'''
This module defines a base class for output methods that are used to get shell output from the
remote machine to the attacker's host.
'''

import sys
import signal

class Output:
    '''
    Base class for output methods that are used to get shell output from the remote machine to the
    attacker's host.
    '''

    def __init__(self):
        self.init_exfil()

    def init_exfil(self):
        '''
        Can be overwritten by derived class.

        Called before shell output is sent to the client for the first time.
        Useful, e.g., to initialize a socket.
        '''

    def deinit_exfil(self):
        '''
        Can be overwritten by derived class.

        Called before disconnecting from the remote shell.
        Useful, e.g., for cleaning up.
        '''

    def recv_output(self):
        '''
        Needs to be overwritten in derived class.

        Called whenever new output is received.
        '''

        return b''

    def handle_shell_output(self):
        '''
        Receiving thread. Read output from shell on server.
        '''

        while True:
            data = self.recv_output()

            if data == b'asdfasdfasdfexitasdfasdfasdf':
                signal.raise_signal(signal.SIGINT)

            sys.stdout.buffer.write(data)
            sys.stdout.flush()
