'''
This module defines an output method that uses an UDP listener to receive shell output.
'''

import socket

from .abstract import Output

class UDPOutput(Output):
    '''
    Output method that uses an UDP listener to receive shell output.
    '''

    def __init__(self, port):
        self.listen_port = port
        super().__init__()

    # Data exfiltration
    def recv_output(self):
        data, _ = self.shell_output_sock.recvfrom(508)
        return data

    def init_exfil(self):
        self.shell_output_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.shell_output_sock.bind(('0.0.0.0', self.listen_port))

    def deinit_exfil(self):
        self.shell_output_sock.close()
