'''
This module defines output methods that can be used to get the output of shell commands from the
remote server.
'''

__all__ = [
    'ping',
    'udp',
]
