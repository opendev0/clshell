# Not Working

* Backspace (0x7f) only deletes chars from buffer, but not from terminal
* Arrow key is not interpreted on server, because tty echo mode is off

# Autokill

* Implement timer in extra thread and parameter to kill the program automatically after specified time

# Kill-Cmds

* Extra-Parameter to specify commands executed when the program exits (e.g. delete webshell)

# Synchronize rows and cols of local TTY

* Enable the user to sync TTY size manually (e.g. because he changed terminal size)

# CLI parameters

Get some values as command line arguments:

* Shell that shall be executed (currently /bin/sh is started)
* Path where the input pipe shall be created

# Control commands

Add some possibility to send control commands.

E.g. TTY rows and cols, upload/download files.

Possibilties:

## Possibility 1

* Use second pipe instead of sending everything to cmd pipe directly.
* Might prevent from overwriting valid commands.

## Possibility 2

* Special commands on client-side
