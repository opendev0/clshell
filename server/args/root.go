package args

import (
	"flag"

	"gitlab.org/opendev0/clshell/server/output"
)

type RootArgs struct {
	PipePath string
}

func Parse() (ra RootArgs, oh output.OutputHandler) {
	flag.StringVar(&ra.PipePath, "pipe-path", "/tmp/cmd_pipe", "Path of input pipe. Should be writable.")
	flag.Parse()

	if len(flag.Args()) < 1 {
		usage()
	}

	ohCreator, ok := subCmds[flag.Args()[0]]
	if !ok {
		usage()
	}

	oh = ohCreator(flag.Args()[1:])

	return
}
