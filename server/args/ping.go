package args

import (
	"flag"

	"gitlab.org/opendev0/clshell/server/output"
)

func init() {
	subCmds["ping"] = NewPingHandler
}

func NewPingHandler(args []string) output.OutputHandler {
	pingCmd := flag.NewFlagSet("ping", flag.ExitOnError)
	serverIP := pingCmd.String("ip", "", "(required) IP address of the attacking machine that will receive the shell output.")
	pingCmd.Parse(args)

	if *serverIP == "" {
		cmdUsage(pingCmd)
	}

	return output.NewPingHandler(*serverIP)
}
