package args

import (
	"flag"
	"fmt"
	"os"

	"gitlab.org/opendev0/clshell/server/output"
)

// subCmds is used by sub-commands to register themselves in their init functions
type ohCreator func([]string) output.OutputHandler
var subCmds = map[string]ohCreator{}

func usage() {
	fmt.Println("You need to specify a sub-command. Following sub-commands are available:")
	for cmd, _ := range subCmds {
		fmt.Println("* " + cmd)
	}
	os.Exit(1)
}

func cmdUsage(fs *flag.FlagSet) {
	fmt.Printf("Parameters for sub-command \"%s\":\n", fs.Name())
	fs.PrintDefaults()
	os.Exit(1)
}
