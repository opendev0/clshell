// +build linux

package main

import (
	"fmt"
	"os"
	"io"
	"os/exec"
	"os/signal"
	"syscall"

	"gitlab.org/opendev0/clshell/server/args"
	"gitlab.org/opendev0/clshell/server/utils"
	"gitlab.org/opendev0/clshell/server/transport"

	"github.com/creack/pty"
)

func runShell(shellPath string) *os.File {
	shell := exec.Command(shellPath)
	shellPtyMaster, err := pty.Start(shell)
	utils.ExitIfError(err)

	syncShell(shellPath, shellPtyMaster)

	return shellPtyMaster
}

func syncShell(shellPath string, shellPtyMaster *os.File) {
	if shellPath == "/bin/bash" {
		// Disable bash history
		shellPtyMaster.Write([]byte("unset HISTFILE\n"))
	}

	// Disable viminfo
	_, err := shellPtyMaster.Write([]byte("alias vim='vim -i NONE'\n"))
	utils.ExitIfError(err)
}

func runCommandReader(cmdPipeFilepath string, shellPtyMaster *os.File) {
	for {
		// Linux named pipes:
		// open will block until data is received.
		// When echo is finished, we receive a EOT. At this point Copy will return.
		// New read operations would return 0 bytes in an endless loop, so close and reopen the pipe to block again.
		cmdPipeFd, _ := os.Open(cmdPipeFilepath)

		var buf [1024]byte
		for {
			nr, err := cmdPipeFd.Read(buf[:])
			if nr == 0 && err != nil {
				break
			}
			if nr == 1 && (buf[0] == 3 || buf[0] == 4) {
				// Only stop if a single byte SIGINT or EOT was sent by the client.
				fmt.Println("Received stop command. Sending SIGTERM to process.")
				syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
				return
			}
			_, err = shellPtyMaster.Write(buf[:nr])
			utils.ExitIfError(err)
		}

		// TODO: Is this needed for medium TTYs?
		// for _, b := range buf {
		// 	fmt.Printf("Received %d\n", b)
		// 	if b == 3 || b == 4 {
		// 		// Stop if SIGINT (CTRL+c) or EOT (CTRL+d) was received
		// 		fmt.Println("Sent SIGTERM")
		// 		syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
		// 		return
		// 	}

		// 	shellPtyMaster.Write(buf[:n])
		// }

		// io.Copy(shellPtyMaster, cmdPipeFd)
		cmdPipeFd.Close()
	}
}

func main() {
	// Handle CLI parameters
	rootArgs, outputHandler := args.Parse()

	// TODO: Implement transport properly
	// Maybe give OutputHandler to Transport?
	transp := transport.NewNoneTransport()
	fmt.Printf("%s", transp.Pack([]byte{1, 2}, 4))

	// Run the shell.
	shellPtyMaster := runShell("/bin/bash")
	defer shellPtyMaster.Close()
	shellPtyMaster.Write([]byte("stty -echo\n"))

	// Initialize shell input via named pipe
	cmdPipeFilepath := rootArgs.PipePath
	cmdPipeFilepath = utils.CreateNamedPipe(cmdPipeFilepath)
	defer os.Remove(cmdPipeFilepath)
	fmt.Printf("Created pipe \"%s\"\n", cmdPipeFilepath)

	// Initialze shell output via chosen method
	outputHandler.Init()
	defer outputHandler.Deinit()

	// Start input and output forwarding to and from the shell
	go runCommandReader(cmdPipeFilepath, shellPtyMaster)
	go io.Copy(outputHandler, shellPtyMaster)

	// Tell the client which pipe he should send commands to
	outputHandler.Write([]byte(fmt.Sprintf("Hello, your command pipe is %s\n", cmdPipeFilepath)))

	// Setup signal handler to ensure that the program can stop cleanly
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)
	<-signalChannel
}
