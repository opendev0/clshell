module gitlab.org/opendev0/clshell/server

go 1.15

require (
	github.com/creack/pty v1.1.11
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
)
