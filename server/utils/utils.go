// +build linux

package utils

import (
	"fmt"
	"log"
	"os"
	"syscall"
)

func ExitIfError(err error) {
	if err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}
}

// Creates the first pipe that does not already exist and returns its filename.
func CreateNamedPipe(cmdPipeFilepath string) (actualPath string) {
	var num int

	for {
		actualPath = fmt.Sprintf("%s-%d", cmdPipeFilepath, num)
		err := syscall.Mkfifo(actualPath, 0600)
		if err == nil {
			break
		}
		if !os.IsExist(err) {
			ExitIfError(err)
		}

		num++
	}

	return
}
