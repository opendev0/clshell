package output

type OutputHandler interface {
	Init()
	Deinit()
	Write(output []byte) (int, error)

	// Length of 0 means no maximum length
	MaxBytes() uint32
}
