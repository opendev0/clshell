package output

import (
	"net"
	"fmt"
	"log"
	"os"
)

type SimpleUDPOutputHandler struct {
	serverIP string
	serverPort uint
	socket net.Conn
}

func NewUDPHandler(serverIP string, serverPort uint) *SimpleUDPOutputHandler {
	return &SimpleUDPOutputHandler{serverIP: serverIP, serverPort: serverPort}
}

func(h *SimpleUDPOutputHandler) Init() {
	fmt.Printf("Initializing UDP socket to %s:%d\n", h.serverIP, h.serverPort)

	serverAddr, _ := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", h.serverIP, h.serverPort))
	conn, err := net.DialUDP("udp", nil, serverAddr)
	h.socket = conn
	if err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}
}

func(h *SimpleUDPOutputHandler) Deinit() {
	fmt.Printf("Deinitializing UDP socket to %s:%d\n", h.serverIP, h.serverPort)

	h.socket.Close()
}

func(h *SimpleUDPOutputHandler) Write(data []byte) (int, error) {
	return h.socket.Write(data)
}

func(h *SimpleUDPOutputHandler) MaxBytes() uint32 {
	return 0
}
