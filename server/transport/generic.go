package transport

/**
 * data: Bytes that should be sent
 * maxBytes: Maximum number of bytes that the output method can send in one packet (e.g., when using the standard Unix `ping` program to exfiltrate data, only 17 bytes can be sent in one packet)
 **/
type Transport interface {
	Pack(data []byte, maxBytes uint32) []byte
}
