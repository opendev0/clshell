package transport

type NoneTransport struct { }

func NewNoneTransport() *NoneTransport {
	return &NoneTransport{}
}

func(t *NoneTransport) Pack(data []byte, maxBytes uint32) []byte {
	return data;
}
